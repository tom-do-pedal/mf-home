import { createApp } from "vue";

import "./index.css";
import "tom_do_pedal_ui/CommomStyles";

import HomeApp from "./HomeApp.vue";

export const mount = (selector: string | HTMLElement) => {
  const rootElement = typeof selector === 'string' ? document.querySelector(selector) : selector
  const app = createApp(HomeApp)
  app.mount(rootElement as HTMLElement);
}

const DEV_ROOT_SELECTOR = '#app-home'
if (process.env.NODE_ENV === 'development') {
  const devRoot = document.querySelector(DEV_ROOT_SELECTOR);
  if (devRoot) {
    mount(DEV_ROOT_SELECTOR);
  }
}